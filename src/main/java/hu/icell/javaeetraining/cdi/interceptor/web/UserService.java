package hu.icell.javaeetraining.cdi.interceptor.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@Path("/users")
public class UserService {

	@Context
	private UriInfo uriInfo;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers() {

		List<User> users = new ArrayList<User>(UserDAO.MAP.values());
		return Response.status(Response.Status.OK).entity(users).build();

	}
	

	@GET
	@Path("{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByName(@PathParam("username") String username) {

		List<User> users = new ArrayList<User>(UserDAO.MAP.values());

		for (User u : users) {
			if (u.getUsername().equals(username)) {
				return Response.status(Response.Status.OK).entity(u).build();
			}
		}

		return Response.status(Status.NOT_FOUND).header("message", "User with the specific name not found: " + username)
				.build();

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(User contact) throws URISyntaxException {

		if (UserDAO.MAP.containsKey(contact.getUsername())) {
			return Response.status(Response.Status.CONFLICT)
					.header("message", "User already exists with this name" + contact.getUsername()).build();
		}

		UserDAO.MAP.put(contact.getUsername(), contact);
		URI location = new URI(uriInfo.getRequestUri() + "/" + contact.getUsername());
		return Response.status(Response.Status.CREATED).location(location).build();

	}
}
