package hu.icell.javaeetraining.cdi.interceptor.web;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserDAO {

	public static final Map<String, User> MAP = new HashMap<String, User>();

	static {
		for (int i = 0; i < 100; i++) {
			User u = new User();
			u.setUsername("aba_" + i + "_" + UUID.randomUUID().toString());
			u.setAge(((int) (Math.random() * 100)) + 1);
			MAP.put(u.getUsername(), u);
		}
	}

}
