package hu.icell.javaeetraining.cdi.interceptor.web;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class UserApplication extends Application {

	
	@Override
	public Set<Class<?>> getClasses() {
		
		Set<Class<?>> classes = new HashSet<>();
		classes.add(UserService.class);
		
		return classes;
	}

}
