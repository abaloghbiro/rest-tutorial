package hu.icell.javaeetraining.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/IcellQueue") })
public class ICellMessageListener implements MessageListener {

	@Override
	public void onMessage(Message message) {

		if (message instanceof TextMessage) {

			TextMessage m = (TextMessage) message;

			try {
				System.out.println("Message received: " + m.getText()+ " / LISTENER IS : "+toString());
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
