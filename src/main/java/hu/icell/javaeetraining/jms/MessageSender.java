package hu.icell.javaeetraining.jms;

import java.io.IOException;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/send" })
public class MessageSender extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:/ConnectionFactory")
	private ConnectionFactory connFactory;

	@Resource(lookup = "java:/jms/queue/IcellQueue")
	private Destination destination;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			QueueConnection connection = (QueueConnection) connFactory.createConnection();

			QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			MessageProducer producer = session.createProducer(destination);

			TextMessage message = session.createTextMessage();
			message.setText("Hello JMS!");

			producer.send(message);

			resp.getWriter().println("Message sent: " + message.getText());

		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
